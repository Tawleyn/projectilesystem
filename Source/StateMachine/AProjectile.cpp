// Fill out your copyright notice in the Description page of Project Settings.

#include "AProjectile.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->InitSphereRadius(55.0f);
	SetRootComponent(SphereComp);

	SphereComp->SetSimulatePhysics(false);

	BuildMovementComponent();
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectile::Deactivate()
{
	InstancedMesh->RemoveMeshFromVisibility(MyIndex);
	GetWorldTimerManager().ClearTimer(LifespanTimer);
	SetActive(false);
}

void AProjectile::BuildMovementComponent()
{
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->SetUpdatedComponent(SphereComp);
	ProjectileMovementComponent->InitialSpeed = 3000.0f;
	ProjectileMovementComponent->MaxSpeed = 3000.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = true;
	ProjectileMovementComponent->Bounciness = 0.3f;
	ProjectileMovementComponent->ProjectileGravityScale = 0.0f;

}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	InstancedMesh->SetMeshTransform(MyIndex, GetActorLocation());
}

void AProjectile::SetLifeSpan(float NewLifespan)
{
	Lifespan = NewLifespan;
	GetWorldTimerManager().SetTimer(LifespanTimer, this, &AProjectile::Deactivate, Lifespan, false);
}

void AProjectile::SetFireVelocity(const FVector& ShootDirection)
{
	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}

void AProjectile::SetIndex(int NewIndex)
{
	MyIndex = NewIndex;
}

void AProjectile::SetMeshID(int NewID)
{
	MeshID = NewID;
}

void AProjectile::FindMesh()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AInstancedProjectileMesh::StaticClass(), FoundActors);

	for (AActor* FoundActor : FoundActors)
	{
		AInstancedProjectileMesh* Casted = Cast<AInstancedProjectileMesh>(FoundActor);
		if (MeshID == Casted->GetMeshID())
		{
			InstancedMesh = Cast<AInstancedProjectileMesh>(FoundActor);
		}
	}
}

void AProjectile::SetActive(bool bActive)
{
	Active = bActive;
	SetActorHiddenInGame(!bActive);
	ProjectileMovementComponent->SetActive(bActive);
	SetActorTickEnabled(bActive);
}

bool AProjectile::IsActive()
{
	return Active;
}