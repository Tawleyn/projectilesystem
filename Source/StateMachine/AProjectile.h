// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InstancedProjectileMesh.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AProjectile.generated.h"

UCLASS()
class STATEMACHINE_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int MyIndex;
	int MeshID = 0;

	float Lifespan = 5.0f;
	FTimerHandle LifespanTimer;
	bool Active;

	UFUNCTION(BlueprintCallable)
		void Deactivate();

	void BuildMovementComponent();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class USphereComponent* SphereComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* SphereMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UParticleSystemComponent* ParticleComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(VisibleAnywhere)
		AInstancedProjectileMesh* InstancedMesh;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void SetLifeSpan(float NewLifespan) override;

	void SetFireVelocity(const FVector& ShootDirection);
	void SetIndex(int NewIndex);
	void SetMeshID(int NewID);
	void FindMesh();
	void SetActive(bool bActive);

	bool IsActive();
};
