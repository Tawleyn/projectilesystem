// Fill out your copyright notice in the Description page of Project Settings.


#include "DispenserWall.h"
#include "Components/BoxComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ADispenserWall::ADispenserWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CubeComp = CreateDefaultSubobject<UBoxComponent>(TEXT("CubeComp"));
	CubeComp->SetBoxExtent(FVector(25.0f, 1000.0f, 450.0f));
	SetRootComponent(CubeComp);

	ProjectilePooler = CreateDefaultSubobject<UPool>(TEXT("Pooler"));
}

// Called when the game starts or when spawned
void ADispenserWall::BeginPlay()
{
	Super::BeginPlay();
	
	Spawn();
	FireLineTrace();
}

// Called every frame
void ADispenserWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADispenserWall::Spawn()
{
	if (StartSpawning)
	{
		FVector CentralSpawnPoint = GetActorLocation() + (GetActorForwardVector() * FVector(0.0f, 10.0f, 0.0f));

		for (int i = -SpreadWidth; i <= SpreadWidth; i++)
		{
			for (int j = -SpreadHeight; j <= SpreadHeight; j++)
			{
				FVector SpawnPoint = FVector(
					CentralSpawnPoint.X + (i * SpreadSpacing * FMath::Cos(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
					CentralSpawnPoint.Y + (i * SpreadSpacing * FMath::Sin(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
					CentralSpawnPoint.Z);
				SpawnPoint += FVector(0.0f, 0.0f, SpreadSpacing * j);

				AProjectile* PoolableProjectile = ProjectilePooler->GetPooledProjectile();

				if (PoolableProjectile == nullptr)
				{
					ProjectilePooler->AddPojectileToPool();
					PoolableProjectile = ProjectilePooler->GetPooledProjectile();
				}

				PoolableProjectile->SetActorLocation(SpawnPoint);
				PoolableProjectile->SetLifeSpan(LifeSpan);
				PoolableProjectile->SetActive(true);
				PoolableProjectile->SetActorRotation(GetActorRotation());
				PoolableProjectile->SetFireVelocity(GetActorRotation().Vector());
			}
		}

	}
	GetWorldTimerManager().SetTimer(SpawnCooldownTimer, this, &ADispenserWall::Spawn, SpawnCooldown, false);
}

void ADispenserWall::FireLineTrace()
{
	if (StartLineTrace)
	{
		FHitResult OutHit;
		FVector CentralSpawnPoint = GetActorLocation() + (GetActorForwardVector() * FVector(0.0f, 100.0f, 0.0f));
		FCollisionQueryParams CollisionParams;

		for (int i = -SpreadWidth; i <= SpreadWidth; i++)
		{
			for (int j = 0; j <= SpreadHeight; j++)
			{
				FVector SpawnPoint = FVector(
					CentralSpawnPoint.X + (i * SpreadSpacing * FMath::Cos(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
					CentralSpawnPoint.Y + (i * SpreadSpacing * FMath::Sin(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
					CentralSpawnPoint.Z);
				SpawnPoint += FVector(0.0f, 0.0f, SpreadSpacing * j);

				FVector EndPoint = SpawnPoint + (GetActorForwardVector() * TraceRange);

				if (GetWorld()->LineTraceSingleByChannel(OutHit, SpawnPoint, EndPoint, ECC_Visibility, CollisionParams))
				{
					if (OutHit.bBlockingHit)
					{
						FTransform SpawnTransform;
						DrawDebugLine(GetWorld(), SpawnPoint, EndPoint, FColor::Green, false, 1, 0, 5);
						SpawnTransform.SetLocation(OutHit.ImpactPoint);

						UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystemRef, SpawnTransform, true, EPSCPoolMethod::AutoRelease, true);
					}
				}
			}
		}
	}
	GetWorldTimerManager().SetTimer(LineTraceCooldownTimer, this, &ADispenserWall::FireLineTrace, TraceCooldown, false);
}

