// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pool.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DispenserWall.generated.h"

UCLASS()
class STATEMACHINE_API ADispenserWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADispenserWall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
		bool StartSpawning = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
		bool StartLineTrace = true;

	FTimerHandle SpawnCooldownTimer;
	FTimerHandle LineTraceCooldownTimer;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UBoxComponent* CubeComp;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		UPool* ProjectilePooler;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
		int SpreadWidth = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
		int SpreadHeight = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
		float SpreadSpacing = 100.0f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float TraceRange = 10000.0f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float LifeSpan = 5.0f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float SpawnCooldown = 1.2f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float TraceCooldown = 1.2f;

	UPROPERTY(EditAnywhere)
		UParticleSystem* ParticleSystemRef;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Spawn();

	void FireLineTrace();
};
