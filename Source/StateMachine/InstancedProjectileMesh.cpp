// Fill out your copyright notice in the Description page of Project Settings.


#include "InstancedProjectileMesh.h"
#include "Components/SphereComponent.h"

// Sets default values
AInstancedProjectileMesh::AInstancedProjectileMesh()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	InstancedProjectileComponent = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("InstancedProjectileComponent"));
	SetRootComponent(InstancedProjectileComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh>SphereMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));	
	SphereMesh = SphereMeshAsset.Object;

	static ConstructorHelpers::FObjectFinder<UMaterial> SphereMaterialAsset(TEXT("Material'/Engine/BasicShapes/BasicShapeMaterial.BasicShapeMaterial'"));
	SphereMesh->SetMaterial(0, SphereMaterialAsset.Object);

	InstancedProjectileComponent->SetStaticMesh(SphereMesh);
}

// Called when the game starts or when spawned
void AInstancedProjectileMesh::BeginPlay()
{
	Super::BeginPlay();
}

void AInstancedProjectileMesh::SetMeshTransform(int Index, FVector ComponentLocation)
{
	if (InstancedProjectileComponent->GetInstanceCount() < Index - 1)
	{
		InstancedProjectileComponent->AddInstance(FTransform(FVector(0.0f, 0.0f, 0.0f)));
	}
	InstancedProjectileComponent->UpdateInstanceTransform(Index, FTransform(ComponentLocation), true, true, true);
}

void AInstancedProjectileMesh::RemoveMeshFromVisibility(int Index)
{
	FTransform NewScale;
	NewScale.SetScale3D(FVector::ZeroVector);
	InstancedProjectileComponent->UpdateInstanceTransform(Index, NewScale, true, true, true);
}

int AInstancedProjectileMesh::GetMeshID()
{
	return MeshID;
}
