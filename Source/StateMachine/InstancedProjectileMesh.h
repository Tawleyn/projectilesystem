// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InstancedProjectileMesh.generated.h"

UCLASS()
class STATEMACHINE_API AInstancedProjectileMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInstancedProjectileMesh();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		int MeshID = 0;

	UPROPERTY(VisibleAnywhere)
		UInstancedStaticMeshComponent* InstancedProjectileComponent;

	UStaticMesh* SphereMesh;

public:	
	void SetMeshTransform(int Index, FVector ComponentLocation);
	void RemoveMeshFromVisibility(int Index);

	int GetMeshID();
};
