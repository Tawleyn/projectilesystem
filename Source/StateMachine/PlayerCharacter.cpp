// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

#include "Components/CapsuleComponent.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetMesh()->SetRelativeLocationAndRotation(FVector(0.0f, 10.0f, -90.0f), FQuat(FRotator(0.0f, -90.0f, 0.0f)));

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->SetupAttachment(RootComponent);
	SpringArmComp->TargetArmLength = 3000.0f;
	SpringArmComp->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));
	SpringArmComp->bUsePawnControlRotation = false;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp, USpringArmComponent::SocketName);
	CameraComp->bUsePawnControlRotation = false;
	CameraComp->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	CameraComp->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	ProjectilePooler = CreateDefaultSubobject<UPool>(TEXT("Pooler"));
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateTowardMouse();

	if (bFiring)
	{
		FireProjectile();
	}

	if (bLineTracing)
	{
		FireLineTrace();
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Fire01", IE_Pressed, this, &APlayerCharacter::SwitchFiringProjectiles);
	PlayerInputComponent->BindAction("Fire01", IE_Released, this, &APlayerCharacter::SwitchFiringProjectiles);

	PlayerInputComponent->BindAction("Fire02", IE_Pressed, this, &APlayerCharacter::SwitchFiringLineTrace);
	PlayerInputComponent->BindAction("Fire02", IE_Released, this, &APlayerCharacter::SwitchFiringLineTrace);
}

void APlayerCharacter::MoveForward(float AxisValue) 
{
	if ((Controller != nullptr) && (AxisValue != 0.0f))
	{
		FRotator Rotation = Controller->GetControlRotation();
		FRotator YawRotation(0, Rotation.Yaw, 0);

		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, AxisValue);
	}
}

void APlayerCharacter::MoveRight(float AxisValue)
{
	if ((Controller != nullptr) && (AxisValue != 0.0f))
	{
		FRotator Rotation = Controller->GetControlRotation();
		FRotator YawRotation(0, Rotation.Yaw, 0);

		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, AxisValue);
	}
}

void APlayerCharacter::RotateTowardMouse()
{
	FVector MouseLocation, MouseDirection;
	Controller->GetWorld()->GetFirstPlayerController()->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);

	FVector ActorLocation = GetActorLocation();
	FVector EndLocation = FMath::LinePlaneIntersection(MouseLocation, MouseLocation + (MouseDirection * 10000.f), ActorLocation, FVector{ 0.0f, 0.0f, 1.0f });

	FRotator ActorRotation = GetActorRotation();
	ActorRotation.Yaw = (EndLocation - ActorLocation).Rotation().Yaw;

	SetActorRotation(ActorRotation);

	DrawDebugLine(GetWorld(), GetActorLocation(), EndLocation, FColor::Red, false, 0.0f, 0, 5.f);
}

void APlayerCharacter::SwitchFiringProjectiles()
{
	bFiring = !bFiring;
}

void APlayerCharacter::SwitchFiringLineTrace()
{
	bLineTracing = !bLineTracing;
}

void APlayerCharacter::FireProjectile()
{
	// Fire a bullet
	FVector CentralSpawnPoint = GetActorLocation() + (GetActorForwardVector() * FireOffset);

	for (int i = -SpreadWidth; i <= SpreadWidth; i++)
	{
		for (int j = 0; j <= SpreadHeight; j++)
		{
			FVector SpawnPoint = FVector(
				CentralSpawnPoint.X + (i * SpreadSpacing * FMath::Cos(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
				CentralSpawnPoint.Y + (i * SpreadSpacing * FMath::Sin(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
				CentralSpawnPoint.Z);
			SpawnPoint += FVector(0.0f, 0.0f, SpreadSpacing * j);

			Spawn(SpawnPoint);
		}
	}
}

void APlayerCharacter::FireLineTrace()
{
	FHitResult OutHit;
	FVector CentralSpawnPoint = GetActorLocation() + (GetActorForwardVector() * FireOffset);
	FCollisionQueryParams CollisionParams;

	for (int i = -SpreadWidth; i <= SpreadWidth; i++)
	{
		for (int j = 0; j <= SpreadHeight; j++)
		{
			FVector SpawnPoint = FVector(
				CentralSpawnPoint.X + (i * SpreadSpacing * FMath::Cos(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
				CentralSpawnPoint.Y + (i * SpreadSpacing * FMath::Sin(FMath::DegreesToRadians(GetActorRotation().Yaw + 90))),
				CentralSpawnPoint.Z);
			SpawnPoint += FVector(0.0f, 0.0f, SpreadSpacing * j);

			FVector EndPoint = SpawnPoint + (GetActorForwardVector() * TraceRange);

			if (GetWorld()->LineTraceSingleByChannel(OutHit, SpawnPoint, EndPoint, ECC_Visibility, CollisionParams))
			{
				if (OutHit.bBlockingHit)
				{
					FTransform SpawnTransform;
					DrawDebugLine(GetWorld(), SpawnPoint, EndPoint, FColor::Green, false, 1, 0, 1);
					SpawnTransform.SetLocation(OutHit.ImpactPoint);

					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystemRef, SpawnTransform, true, EPSCPoolMethod::AutoRelease, true);
				}
			}
		}
	}					
}

void APlayerCharacter::Spawn(FVector SpawnPoint)
{
	AProjectile* PoolableProjectile = ProjectilePooler->GetPooledProjectile();

	if (PoolableProjectile == nullptr)
	{
		ProjectilePooler->AddPojectileToPool();
		PoolableProjectile = ProjectilePooler->GetPooledProjectile();
	}

	PoolableProjectile->SetActorLocation(SpawnPoint);
	PoolableProjectile->SetLifeSpan(LifeSpan);
	PoolableProjectile->SetActive(true);
	PoolableProjectile->SetActorRotation(GetActorRotation());
	PoolableProjectile->SetFireVelocity(GetActorRotation().Vector());
}
