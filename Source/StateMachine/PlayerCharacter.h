// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "AProjectile.h"
#include "Pool.h"

#include "PlayerCharacter.generated.h"

UCLASS()
class STATEMACHINE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	void Spawn(FVector SpawnPoint);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void FireProjectile();
	void FireLineTrace();

	void SwitchFiringProjectiles();
	void SwitchFiringLineTrace();

	void MoveForward(float InputAxis);
	void MoveRight(float InputAxis);

	void RotateTowardMouse();

	bool bFiring = false;
	bool bLineTracing = false;

protected: 
	UPROPERTY(VisibleAnywhere)
		class USpringArmComponent* SpringArmComp;
	
	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* CameraComp;

	UPROPERTY(EditAnywhere)
		FVector FireOffset;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float LifeSpan = 5.0f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float SpawnCooldown = 1.2f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		int SpreadWidth = 0;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		int SpreadHeight = 0;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float SpreadSpacing = 100.0f;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		float TraceRange = 10000.0f;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* ParticleSystemRef;

	UPROPERTY(EditAnywhere, Category = "Spawner")
		UPool* ProjectilePooler;

private:
	FTimerHandle SpawnCooldownTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
