// Fill out your copyright notice in the Description page of Project Settings.

#include "Pool.h"

// Sets default values for this component's properties
UPool::UPool()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}


// Called when the game starts
void UPool::BeginPlay()
{
	Super::BeginPlay();
	for (int i = 0; i < PoolSize; i++)
	{
		AddPojectileToPool();
	}
}

AProjectile* UPool::GetPooledProjectile()
{
	for (AProjectile* PoolableActor : Pool)
	{
		if (!PoolableActor->IsActive())
		{
			return PoolableActor;
		}
	}
	return nullptr;
}

void UPool::AddPojectileToPool()
{
	if (PooledProjectileSubclass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World)
		{
			AProjectile* PoolableProjectile = World->SpawnActor<AProjectile>(PooledProjectileSubclass, FVector::ZeroVector, FRotator::ZeroRotator);

			PoolableProjectile->SetActive(false);
			PoolableProjectile->SetIndex(Pool.Num());
			PoolableProjectile->SetMeshID(MeshID);
			PoolableProjectile->FindMesh();
			Pool.Add(PoolableProjectile);
		}
	}
}
