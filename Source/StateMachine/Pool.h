// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AProjectile.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Pool.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class STATEMACHINE_API UPool : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPool();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	AProjectile* GetPooledProjectile();

	void AddPojectileToPool();

	UPROPERTY(EditAnywhere, Category = "ObjectPooler")
		TSubclassOf<class AProjectile> PooledProjectileSubclass;

	UPROPERTY(EditAnywhere, Category = "ObjectPooler")
		int PoolSize = 100;

	UPROPERTY(EditAnywhere, Category = "ObjectPooler")
		int MeshID = 0;

	TArray<AProjectile*> Pool;		
};
